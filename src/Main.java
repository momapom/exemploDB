import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) throws SQLException {

  		Materia materia = new Materia();
//  		materia.setMateria("Fisica");
//		materia.setAreaConhecimento("Exatas");
		
//		MateriaDao criarMateria = new MateriaDao();
//		criarMateria.inserirMateria(materia);
//
		MateriaDao consultar = new MateriaDao();
		
		List<Materia> materias = consultar.consultarMaterias();
		for (Materia materia2 : materias) {
			System.out.println("Materia " + materia2.getMateria());
			
			System.out.println("Area Conhecimento " + materia2.getAreaConhecimento());
		}

//		MateriaDao excluir = new MateriaDao();
//		excluir.excluirMateria("Matematica");

//		MateriaDao alterarArea = new MateriaDao();
//		alterarArea.alterarMateria("Matematica", "Humanas");

	}
}
