
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MateriaDao {
	private Connection conexao;

	
	public MateriaDao() {
		this.conexao = Conexao.getConnection();
	}
	public void inserirMateria(Materia materia) {
		String query = "INSERT INTO materia(materia, areaConhecimento)" +
				"VALUES (?,?)";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, materia.getMateria());
			statement.setString(2, materia.getAreaConhecimento());

			statement.execute();
			statement.close();
			System.out.println("Matéria " + materia.getMateria() + " criada com sucesso");

		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}

	public List<Materia> consultarMaterias() {
		List<Materia> materias = new ArrayList<Materia>();

		String query = "SELECT * FROM materia";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);

			ResultSet resultado = statement.executeQuery();
			while (resultado.next()){
				Materia materia = new Materia();
				materia.setMateria(resultado.getString("materia"));
//				System.out.println(resultado.getString("materia"));
				materia.setAreaConhecimento(resultado.getString("areaConhecimento"));
//				System.out.println(resultado.getString("areaConhecimento"));
				materias.add(materia);
			}
			resultado.close();
			statement.close();
			return materias;
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}
	public Materia consultarMateria(String nomeMateria) {

		String query = "SELECT * FROM materia WHERE materia = ?";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, nomeMateria);

			ResultSet resultado = statement.executeQuery();
			resultado.next();
			Materia materia = new Materia();
			materia.setMateria(resultado.getString("materia"));
			materia.setAreaConhecimento(resultado.getString("areaConhecimento"));
			resultado.close();
			statement.close();
			return materia;
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}
	public void excluirMateria(String nomeMateria) {

		String query = "DELETE FROM materia WHERE materia = ?";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, nomeMateria);
			statement.execute();
			statement.close();
			System.out.println("Matéria " + nomeMateria + " excluída");
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}
	public void alterarMateria(String nomeMateria, String areaConhecimento) {

		String query = "UPDATE materia SET areaConhecimento = ? WHERE materia = ?";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, areaConhecimento);
			statement.setString(2, nomeMateria);
			statement.execute();
			statement.close();
			System.out.println("Área de conhecimento da Matéria " + nomeMateria + " foi"+
			     " alterada para " + areaConhecimento);
		}
		catch (SQLException e){
			throw new RuntimeException(e);
		}
	}}
